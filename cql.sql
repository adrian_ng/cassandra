/*
Question 1. [10 marks] List the database write and update requests the
application requires using plain English.


1.Update the password for driver 'pondy'.

2.Driver 'pondy' comes to station to update his current_position ‘Wellington’.

3.Driver  'pondy' is assigned to a service thus to update his current_position ‘FA4567’.

4.Driver 'pondy' deregisters from his work thus to update current_position to  ‘not_available’.

5.Update the number of days the driver 'pondy'  works per month.

6.Update the skill of driver 'pondy' from {'Ganz Mavag','Guliver'} to  {'Ganz Mavag','Guliver', 'Matangi'}.

7.Update the status of a vehicle.

8.Update the total distance travelled for  vehicle ‘FA4567’ .

9.Update the days registered for 'pondy'. 


Question 2. [12 marks] List the read requests the application requires using
plain English.


Extract a list of departure stations with time in descending order

Find the driver in 'Upper Hutt' with the skill 'Guliver'. 

Find the vehicle in 'Upper Hutt'.

Select the last data point for a service on a day 

Select all Data points for a service on a day in a time interval

Get the north and south direction for a given data point .  



Question 3. [9 marks] Consider Cassandra data model design guidelines we
discussed in lectures and list names of database tables the application requires
using plain English. Recall, Cassandra tables strongly depend on requested
queries. If there is no queries needing a table, the table should not exist. (Don’t
invent queries to justify the existence of any tables.) After each table name, list
those queries you identified in your answer to question 2 that use the table.

dispatchAllocation :
extract a list of departure stations with time in descending order

dataPoint :
select the last data point for a service on a day 
select all Data points for a service on a day in a time interval

timeTable:
get the north and south direction for a given data point .  



Question 4. [20 marks] Create data model using CQL 3 statements that support
the requirements. To answer questions, use Cassandra CCM. In your answers,
copy your CCM and CQL commands.
a. [5 marks] Create a cluster and a keyspace that will satisfy infrastructure and
availability requirements above.
b. [15 marks] Define tables listed in your answer to question 3 above. For the
table definitions include any non default property settings. Optimize your
database solution just for iPhone application queries you identified in
question 2 above.

*/
DROP KEYSPACE WellingtonTranzMetro;
CREATE KEYSPACE WellingtonTranzMetro WITH replication = {'class':'SimpleStrategy', 'replication_factor' : 3};
USE WellingtonTranzMetro;

drop TABLE dispatchAllocation ;
CREATE TABLE dispatchAllocation( 
   station  text, 
   vehicle  text, 
   driver  text, 
   services_no int , 
   time int,
   lineName text,  
  PRIMARY KEY ((station, lineName), time) 
  )WITH CLUSTERING ORDER BY (time desc);
INSERT INTO dispatchAllocation (station, vehicle, driver, services_no ,time ,lineName ) VALUES('Wellington', 'FA1122' ,  'milan' , 1, 0605 , 'Hutt Vale Line');
INSERT INTO dispatchAllocation (station, vehicle, driver, services_no ,time ,lineName) VALUES('Wellington', 'FA4864' , 'pondy' ,2, 0617 , 'Hutt Vale Line');
INSERT INTO dispatchAllocation (station, vehicle, driver, services_no ,time ,lineName) VALUES('Wellington',  'FA4567' , 'pavle'  ,3,0620, 'Hutt Vale Line');
INSERT INTO dispatchAllocation (station, vehicle, driver, services_no ,time ,lineName) VALUES('Upper Hutt',  'FA4567' , 'pavle'  ,4,0700, 'Hutt Vale Line');
INSERT INTO dispatchAllocation (station, vehicle, driver, services_no ,time ,lineName) VALUES('Upper Hutt',  'FA4567' , 'pavle'  ,5,0705, 'Hutt Vale Line');

INSERT INTO dispatchAllocation (station, vehicle, driver, services_no ,time ,lineName) VALUES('Upper Hutt',  'FA4567' , 'pavle'  ,6,0710, 'Hutt Vale Line');
select * from dispatchAllocation;


CREATE TABLE dataPoint(
   drv_name text, 
   vehicle_id text,  
   services_no int ,
   lineName text , 
   day int ,
   sequence timestamp ,
   latitude double, 
   longitude double ,
   speed double ,
    PRIMARY KEY (vehicle_id , sequence ) 
      )WITH CLUSTERING ORDER BY (sequence DESC);
INSERT INTO dataPoint (drv_name,vehicle_id, services_no, lineName, day, sequence,latitude,longitude,speed) VALUES('pondy','FA4567', 1,   'Hutt Vale Line',20170325,1490475600000,-41.2262,174.77  ,29.1);
INSERT INTO dataPoint (drv_name,vehicle_id, services_no,lineName,day,sequence,latitude,longitude,speed) VALUES('pondy','FA4567',1, 'Hutt Vale Line',  20170325,  1490475610000,-41.2263 ,175,  70.1);
INSERT INTO dataPoint (drv_name,vehicle_id, services_no,lineName,day,sequence,latitude,longitude,speed) VALUES('pondy','FA4567', 1, 'Hutt Vale Line',  20170325, 1490475620000,-41.2264, 175.07,  80.5);
select * from dataPoint ;




CREATE TABLE timeTable( 
   station_name   text ,
   longitude double,
   latitude double,
   services_no int , 
   distance double ,
   time int,
   lineName text,  
  PRIMARY KEY (( lineName , services_no ) , time) 
  )WITH CLUSTERING ORDER BY (time DESC);

INSERT INTO timeTable (station_name, longitude, latitude,services_no, distance, time ,lineName) VALUES('Wellington',174.7762, -41.2865, 1 ,0, 0605  , 'Hutt Vale Line');
INSERT INTO timeTable (station_name, longitude, latitude,services_no, distance,time ,lineName) VALUES('Petone',    174.8851, -41.227 , 1 , 8.3 ,0617, 'Hutt Vale Line');
INSERT INTO timeTable (station_name, longitude, latitude,services_no, distance, time ,lineName) VALUES('Woburn',  174.9081, -41.211, 1, 11.0,0620,     'Hutt Vale Line' );  -- test row  
INSERT INTO timeTable (station_name, longitude, latitude,services_no, distance, time ,lineName) VALUES('Waterloo',174.7762, -41.2092, 1 ,13.3, 0625  , 'Hutt Vale Line');



CREATE TABLE drivers(
   drv_name text PRIMARY KEY,
   cur_pos text,
   mobile  int,
   pwd text,
   skill set<text>
   );
   CREATE INDEX ON drivers (skill);
INSERT INTO drivers (drv_name, cur_pos, mobile, pwd, skill) VALUES('milan','Upper Hutt', 211111, 'mm77', {'Matangi'});
INSERT INTO drivers (drv_name, cur_pos, mobile, pwd, skill) VALUES('pavle','Upper Hutt', 213344, 'pm33', {'Ganz Mavag','Guliver'}); 
INSERT INTO drivers (drv_name, cur_pos, mobile, pwd, skill) VALUES('pondy','Wellington', 216677, 'pondy', {'Matangi', 'Kiwi Rail'});





CREATE TABLE vehicles(
   vehicle_id text,
   status text,   --  a station name 
   vehicle_type  text,
    PRIMARY KEY(vehicle_id)
   );
INSERT INTO vehicles (vehicle_id, status, vehicle_type ) VALUES('FA1122', 'Upper Hutt', 'Matangi');
INSERT INTO vehicles (vehicle_id, status, vehicle_type) VALUES('FP8899', 'maintenance', 'Ganz Mavag');
INSERT INTO vehicles (vehicle_id, status, vehicle_type) VALUES('FA4864', 'Wellington', 'Matangi');
INSERT INTO vehicles (vehicle_id, status, vehicle_type) VALUES('FA4567', 'Waterloo', 'Matangi');  -- test row 



CREATE TABLE WellingtonTranzMetro.days_registered_counts
  (counter_value counter,  -- counter column   total days in one month; 
  drv_name text, 
  PRIMARY KEY (drv_name)
);



CREATE TABLE vehicles_distance_counts (vehicle_id text, date int, 
total_count counter static, 
daily_count counter, 
PRIMARY KEY( vehicle_id, date));


-- Optimize database solution just for iPhone application queries 
CREATE INDEX latitudeIndex ON dataPoint (latitude);
/*
Question 5. [20 marks] Provide CQL3 statements to support each of the
application write and update requests you specified in Question 1 above. Show
the consistency level before each write and update statement.
*/

--consistency quorum ;
--1.Update the password for driver 'pondy'.
UPDATE drivers SET pwd = 'newPwd' WHERE drv_name='pondy'   ; 

--2.Driver 'pondy' comes to station to update his current_position 'Wellington'.
UPDATE drivers SET cur_pos = 'Waterloo' WHERE drv_name='pondy'   ;  

--3.Driver  'pondy' is assigned to a service thus to update his current_position ‘FA4567’.
UPDATE drivers SET cur_pos = 'FA4567' WHERE drv_name='pondy'   ; 

--4.Update the status of a vehicle.
update   vehicles  set  status =  'in_use'  where  vehicle_id  ='FA4567'   ;

UPDATE vehicles SET status = 'Waterloo' WHERE vehicle_id='FA4567'  ;  

--5.Driver 'pondy' deregisters from his work thus to update current_position to  ‘not_available’.
UPDATE drivers SET cur_pos = 'not_available' WHERE drv_name='pondy'   ; 

--6.Update the number of days the driver 'pondy'  works per month.
UPDATE WellingtonTranzMetro.days_registered_counts SET counter_value = counter_value + 1 WHERE drv_name='pondy';

--7.Update the skill of driver 'pondy' from {'Ganz Mavag','Guliver'} to  {'Ganz Mavag','Guliver', 'Matangi'}.
UPDATE drivers SET skill = skill + {'Guliver'} WHERE drv_name='pondy'   ; 

--8.Update the total distance travelled for  vehicle ‘FA4567’ .
SELECT * FROM vehicles_distance_counts;
UPDATE vehicles_distance_counts SET daily_count = daily_count + 1, total_count = total_count+ 1 WHERE vehicle_id = 'FA4567' AND date = 20160317;
UPDATE vehicles_distance_counts SET daily_count = daily_count + 1, total_count = total_count+ 1 WHERE vehicle_id = 'FA4567' AND date = 20160318;

SELECT * FROM vehicles_distance_counts;


/*
Question 6. [29 marks] Provide CQL3 statements to support each of the
application read requests you specified in Question 2 above. Show the
consistency level before each read statement. In your answer copy your CQL
statement and the result produced by Cassandra from the screen.
*/


--Extract a list of departure stations with time in descending order
SELECT * FROM dispatchAllocation ; 

--Find the driver in 'Wellington' with the skill 'Guliver'. 
select * from drivers where cur_pos='Wellington' and skill CONTAINS  'Matangi' ALLOW FILTERING;

-- Find the vehicle in 'Wellington'. 
select * from vehicles where status='Wellington' allow filtering ;

--Select the last data point for a service on a day. 
select * from dataPoint limit 1;

--Select all Data points for a service on a day in a time interval 10 seconds . 
select * from dataPoint;

--Get the north and south direction neighbours for a given data point  -41.2262 IN services_no 1 and lineName 'Hutt Vale Line' 
-- north_neighbour :
select * from timeTable;
select * from timeTable where latitude > -41.2262  and services_no = 1 and lineName ='Hutt Vale Line'   order by time asc limit 1 ALLOW FILTERING ; 
--  south_neighbour : 
select * from timeTable where latitude < -41.2262 and services_no = 1 and lineName ='Hutt Vale Line'  limit 1  allow filtering ; 










